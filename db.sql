CREATE DATABASE call_billing;

CREATE TABLE `call_billing`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(32) NOT NULL,
  `created_on` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_on` DATETIME NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `user_name_UNIQUE` (`user_name` ASC) VISIBLE);

INSERT INTO `call_billing.users` (user_name) VALUES ('binhkd');
INSERT INTO `call_billing.users` (user_name) VALUES ('namnv');
INSERT INTO `call_billing.users` (user_name) VALUES ('nhatnv');


CREATE TABLE `call_billing`.`call` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `call_duration` BIGINT(20) NULL DEFAULT 0,
  `call_block` BIGINT(20) NULL DEFAULT 0,
  `created_on` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_on` DATETIME NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));
