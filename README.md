# Call Billing
## Setup database
- Database: ***Mysql***
- Chạy query setup database theo query tronng file ***db.sql***

## Run
  1. Config ENV file
  2. Install
  ```
  npm install
  ```
  3. Run
  ```
  npm start
  ```
## Docker run
  1. Config ENV file
  2. Build image
  ```
  sudo docker build -t $IMAGE_NAME:$IMAGE_VERSION -f DockerFile .
  ```
  3. Run container
  ```
  sudo docker run -d --network=host -p 127.0.0.1:9000:9000/tcp $IMAGE_NAME:$IMAGE_VERSION
  ```
## API Document
  ```
  http://HOST:PORT/api-docs/
  ```
