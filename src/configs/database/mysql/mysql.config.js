var AppConfig = require('../../app.config.js');
var mysql = require('mysql');

var con = mysql.createConnection({
  host: AppConfig.DATABASE.HOST,
  user: AppConfig.DATABASE.USER,
  password: AppConfig.DATABASE.PASSWORD,
  database: AppConfig.DATABASE.NAME
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Database connected!!!")
})

module.exports = con;