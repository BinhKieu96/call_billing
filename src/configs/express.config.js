const bodyParse = require('body-parser');
const errorHandler = require('../common/error/ErrorHander');
const routerCall = require('../api/call/controller/call.controller');
var configSwagger = require('../configs/swagger-ui.config');

var configExpress = function (app) {
  app.use(bodyParse.json());
  app.use(bodyParse.urlencoded({extended: true}));
  app.use('/', routerCall);
  app.use(errorHandler);
  configSwagger(app)
}

module.exports = configExpress;