const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../../swagger.json');

var configSwagger = function (app) {
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
}

module.exports = configSwagger;