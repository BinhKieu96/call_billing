var ErrorType = require('./ErrorType');

class ValidationError extends Error {
  constructor(message) {
    super(message);
    this.type = ErrorType.VALIDATION_ERROR;
  }
}

module.exports = ValidationError;