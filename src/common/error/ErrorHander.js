const ErrorType = require('./ErrorType');
const ErrorMessage = require('./ErrorMessage');

var errorHandle = (err, req, res, next) => {
  if(err && err.type == ErrorType.VALIDATION_ERROR) res.status(400).send(err.message);
  else if(err) {
    console.log(err)
    res.status(400).send(ErrorMessage.SYSTEM_ERROR);
  }
}

module.exports = errorHandle;