var userRepos = require('../repository/user.repos');

exports.getUserByName = async (userName) => {
  return await userRepos.getUserByName(userName);
}