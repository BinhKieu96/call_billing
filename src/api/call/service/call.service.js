const callRepos = require('../repository/call.repos');
const userService = require('../../user/service/user.service');
const ValidationError = require('../../../common/error/ValidationError');
const ErrorMessage = require('../../../common/error/ErrorMessage');
const AppConfig = require('../../../configs/app.config');

const BLOCK_SIZE_SECOND = AppConfig.SERVICE.BLOCK_SIZE_SECOND;

exports.saveCall = async (userName, req) => {
  let dataRequest = req.body;

  if(!dataRequest.call_duration) throw new ValidationError(ErrorMessage.CALL_DURATION_NOT_FOUND);
  if(dataRequest.call_duration < 0) throw new ValidationError(ErrorMessage.CALL_DURATION_INVALID);

  let user = await userService.getUserByName(userName);
  if(!user) throw new ValidationError(ErrorMessage.USER_NOT_FOUND);
  
  let blockSizeMilisecond = BLOCK_SIZE_SECOND * 1000;
  let redundantMilisecond = dataRequest.call_duration % blockSizeMilisecond;
  let blockCount = redundantMilisecond == 0 
    ? (dataRequest.call_duration / blockSizeMilisecond) 
    : (((dataRequest.call_duration - redundantMilisecond) / blockSizeMilisecond) + 1);

  let call = {
    user_id: user.id,
    call_duration: dataRequest.call_duration,
    call_block: blockCount
  };

  await callRepos.saveCall(call);
  call.user_name = userName;
  return call;
}

exports.getBill = async (userName, fromDate, toDate) => {
  let user = await userService.getUserByName(userName);
  if(!user) throw new ValidationError(ErrorMessage.USER_NOT_FOUND);
  console.log(fromDate);
  console.log(toDate);
  
  let fromDateQuery;
  let toDateQuery;
  try {
    if(fromDate) fromDateQuery = new Date(Date.parse(fromDate)).toISOString().slice(0, 19).replace('T', ' ');
    if(toDate) toDateQuery = new Date(Date.parse(toDate)).toISOString().slice(0, 19).replace('T', ' ');
  } catch (error) {
    console.log(error);
  }
  
  let data = await callRepos.getBill(user.id, fromDateQuery, toDateQuery);
  return {
    call_count: data ? data.call_count : 0,
    block_count: data ? data.block_count : 0
  }
}