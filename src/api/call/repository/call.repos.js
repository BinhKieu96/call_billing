var sql = require('../../../configs/database/mysql/mysql.config');
const util = require('util');
const executeQuery = util.promisify(sql.query).bind(sql);

exports.saveCall = async (newCall) => {
  await executeQuery(`INSERT INTO call_billing.call (user_id, call_duration, call_block) VALUES (${newCall.user_id}, ${newCall.call_duration}, ${newCall.call_block})`);
};

exports.getBill = async (user_id, fromDate, toDate) => {
  let query = `select count(*) as call_count, sum(call_block) as block_count from call_billing.call where user_id='${user_id}' ` 
    + (fromDate ? `and created_on >= '${fromDate}' ` : "")
    + (toDate ? `and created_on <= '${toDate}' ` : "")
    + `group by user_id `
  let data = await executeQuery(query);
  return data[0];
}