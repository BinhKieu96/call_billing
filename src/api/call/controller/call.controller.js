const express = require('express')
const router = express.Router()
const callService = require('../service/call.service');

router.post('/mobile/:user_name/call', async (req, res, next) => {
  try {
    let data = await callService.saveCall(req.params.user_name, req)
    res.json(data);
  } catch (error) {
    next(error);
  }
});

router.get('/mobile/:user_name/bill', async (req, res, next) => {
  try {
    let data = await callService.getBill(req.params.user_name, req.query.from_date, req.query.to_date);
    res.json(data);
  } catch (error) {
    next(error);
  }
});

module.exports = router;