var express = require('express');
var logger = require('../src/configs/logger.config')

const app = express();
const PORT = 9000;

require('./configs/database');

var configExpress = require('./configs/express.config');
configExpress(app);

app.listen(PORT, () => logger.info(`Server is running on ${PORT}`));

module.exports = app;