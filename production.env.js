module.exports = ENV = {
  NAME: 'production',
  PORT: 9000,
  DATABASE: {
    HOST: 'localhost',
    PORT: 3306,
    USER: 'admin',
    PASSWORD: "admin",
    NAME: 'call_billing'
  },
  SERVICE: {
    BLOCK_SIZE_SECOND: 30
  }
}