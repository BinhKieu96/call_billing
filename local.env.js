module.exports = ENV = {
  NAME: 'local',
  PORT: 9000,
  DATABASE: {
    HOST: 'localhost',
    PORT: 3306,
    USER: 'root',
    PASSWORD: "root",
    NAME: 'call_billing'
  },
  SERVICE: {
    BLOCK_SIZE_SECOND: 30
  }
}