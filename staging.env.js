module.exports = ENV = {
  NAME: 'staging',
  PORT: 9000,
  DATABASE: {
    HOST: 'localhost',
    PORT: 27017,
    USER: 'admin',
    PASSWORD: "admin",
    NAME: 'call_billing'
  },
  SERVICE: {
    BLOCK_SIZE_SECOND: 30
  }
}